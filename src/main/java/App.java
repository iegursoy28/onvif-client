public class App {
    public static void main(String[] args) {
        OnvifClient onvifClient = new OnvifClient();

        if (args.length <= 0) {
            onvifClient.runApp();
        } else if (args.length == 1) {
            if (args[0].equals("-h"))
                showHelper();
            else if (args[0].equals("-d"))
                onvifClient.discover();
        } else {
            showHelper();
        }

        onvifClient.disconnect();
        System.out.println("Exit App");
    }

    private static void showHelper() {
        System.out.println("onvif-client usage:" + "\n\t" +
                "-h : show help" + "\n\t" +
                "-d : discover devices" + "\n");
    }
}

