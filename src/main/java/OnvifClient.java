import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Nonnull;

import be.teletask.onvif.DiscoveryManager;
import be.teletask.onvif.DiscoveryMode;
import be.teletask.onvif.OnvifManager;
import be.teletask.onvif.listeners.DiscoveryListener;
import be.teletask.onvif.listeners.OnvifDeviceInformationListener;
import be.teletask.onvif.listeners.OnvifMediaProfilesListener;
import be.teletask.onvif.listeners.OnvifMediaStreamURIListener;
import be.teletask.onvif.listeners.OnvifResponseListener;
import be.teletask.onvif.listeners.OnvifServicesListener;
import be.teletask.onvif.models.Device;
import be.teletask.onvif.models.OnvifDevice;
import be.teletask.onvif.models.OnvifDeviceInformation;
import be.teletask.onvif.models.OnvifMediaProfile;
import be.teletask.onvif.models.OnvifServices;
import be.teletask.onvif.responses.OnvifResponse;

public class OnvifClient {
    private DiscoveryManager discoveryManager;

    private OnvifManager onvifManager;
    private OnvifDevice onvifDevice = null;

    private static boolean isWaiting = false;

    public OnvifClient() {
        discoveryManager = new DiscoveryManager();
        discoveryManager.setDiscoveryMode(DiscoveryMode.ONVIF);
        discoveryManager.setDiscoveryTimeout(10000);

        onvifManager = new OnvifManager();
        onvifManager.setOnvifResponseListener(onvifResponseListener);
    }

    private static final String appCommandStr = "\nApp Usage" + "\n\t" +
            "1: Create device" + "\n\t" +
            "2: Discover devices" + "\n\t" +
            "3: Get info" + "\n\t" +
            "q: Quit app" + "\n\n" +
            "Select process: ";

    public void runApp() {
        Scanner scanner = new Scanner(System.in);
        String readLine;
        do {
            if (onvifDevice != null)
                System.out.print(String.format("\n\nDevice: %s\n", onvifDevice.getHostName()));
            System.out.print(appCommandStr);
            readLine = scanner.nextLine();

            switch (readLine) {
                case "1":
                    createDevice();
                    break;
                case "2":
                    discover();
                    break;
                case "3":
                    getInfo();
                    break;
                default:
                    System.out.println("Please check input");
                    break;
            }
        } while (!readLine.equals("q"));
    }

    private String getInput(String msg) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(msg + ": ");
        return scanner.nextLine();
    }

    private void createDevice() {
        String host = getInput("Input Host");
        String username = getInput("Input Username");
        String pass = getInput("Input Pass");

        onvifDevice = new OnvifDevice(host, username, pass);
    }


    private void getInfo() {
        if (onvifDevice == null) {
            System.out.println("Create onvif device");
            return;
        }

        if (onvifManager == null) {
            System.out.println("Unknown error");
            return;
        }

        isWaiting = true;
        onvifManager.getDeviceInformation(onvifDevice, new OnvifDeviceInformationListener() {
            @Override
            public void onDeviceInformationReceived(OnvifDevice device, OnvifDeviceInformation deviceInformation) {
                isWaiting = false;
                System.out.println(String.format("Device Info: %s => %s", device.getHostName(), deviceInformation.toString()));
            }
        });
        appWait();

        isWaiting = true;
        onvifManager.getServices(onvifDevice, new OnvifServicesListener() {
            @Override
            public void onServicesReceived(OnvifDevice device, OnvifServices paths) {
                isWaiting = false;
                System.out.println(String.format("Device Info: %s  Paths => \n\tStream path: %s\n\tInfo path: %s\n\tProfiles path: %s\n\tServices path: %s",
                        device.getHostName(), paths.getStreamURIPath(), paths.getDeviceInformationPath(), paths.getProfilesPath(), paths.getServicesPath()));
            }
        });
        appWait();

        isWaiting = true;
        onvifManager.getMediaProfiles(onvifDevice, new OnvifMediaProfilesListener() {
            @Override
            public void onMediaProfilesReceived(OnvifDevice device, List<OnvifMediaProfile> mediaProfiles) {
                System.out.println(String.format("Device Info: %s => Media profile size: %s", device.getHostName(), mediaProfiles.size()));
                for (OnvifMediaProfile profile : mediaProfiles) {
                    onvifManager.getMediaStreamURI(onvifDevice, profile, new OnvifMediaStreamURIListener() {
                        @Override
                        public void onMediaStreamURIReceived(OnvifDevice device, OnvifMediaProfile profile, String uri) {
                            System.out.println(String.format("Device Info: %s => Media Profile: %s, URI: %s",
                                    device.getHostName(), profile.getName(), uri));
                        }
                    });
                }
                isWaiting = false;
            }
        });
        appWait();
    }

    public void disconnect() {
        if (onvifManager != null)
            onvifManager.destroy();
        onvifDevice = null;
    }

    public void discover() {
        isWaiting = true;
        discoveryManager.discover(new DiscoveryListener() {
            @Override
            public void onDiscoveryStarted() {
                System.out.println("Onvif device discovery started");
            }

            @Override
            public void onDevicesFound(List<Device> devices) {
                isWaiting = false;
                for (Device device : devices)
                    System.out.println(String.format("Onvif Devices found: %s username: %s pass: %s",
                            device.getHostName(), device.getUsername(), device.getPassword()));
            }
        });
        appWait();
    }

    private OnvifResponseListener onvifResponseListener = new OnvifResponseListener() {
        @Override
        public void onResponse(OnvifDevice onvifDevice, OnvifResponse response) {
            System.out.print(String.format("Response Onvif Device: %s\tisSuccess: %s\nresp(%s): %s\n",
                    onvifDevice.getHostName(), response.isSuccess(), response.request().getType(), response.getXml()));
        }

        @Override
        public void onError(OnvifDevice onvifDevice, int errorCode, String errorMessage) {
            isWaiting = false;
            System.out.print(String.format("On Error Response Device: %s\terrorCode: %s\nerrorMessage: %s\n",
                    onvifDevice.getHostName(), errorCode, errorMessage));
        }
    };

    private void appWait() {
        System.out.println("please wait");
        while (isWaiting) {
            try {
                Thread.sleep(500);
                System.out.print(".");
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
